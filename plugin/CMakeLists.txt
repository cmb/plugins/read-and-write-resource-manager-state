set(PQPluginHdrs
  pqSMTKResourceManagerStateAutoStart.h
  pqSMTKReadResourceManagerStateBehavior.h
  pqSMTKWriteResourceManagerStateBehavior.h
  )

set(PQPluginSrcs
  pqSMTKResourceManagerStateAutoStart.cxx
  pqSMTKReadResourceManagerStateBehavior.cxx
  pqSMTKWriteResourceManagerStateBehavior.cxx
  )

paraview_plugin_add_auto_start(
  CLASS_NAME pqSMTKResourceManagerStateAutoStart
  STARTUP startup
  SHUTDOWN shutdown
  INTERFACES interfaces
  SOURCES sources
  )

set(CMAKE_AUTOMOC 1)

smtk_add_plugin(smtkReadWriteResourceManagerStatePlugin
  REGISTRAR smtk::resource_manager_state::Registrar
  REGISTRAR_HEADER operators/Registrar.h
  MANAGERS smtk::operation::Manager
  PARAVIEW_PLUGIN_ARGS
    VERSION "1.0"
    REQUIRED_PLUGINS
      smtkPQComponentsPlugin
    UI_INTERFACES
      ${interfaces}
    SOURCES
      ${PQPluginHdrs}
      ${PQPluginSrcs}
      ${sources}
  )

target_link_libraries(smtkReadWriteResourceManagerStatePlugin
  PUBLIC
  smtkCore
  smtkPQComponentsExt
  smtkReadWriteResourceManagerState
  ParaView::pqApplicationComponents
  ParaView::pqComponents
  Qt5::Core
  )
